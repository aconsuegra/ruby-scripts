require 'csv'

puts '<?xml version="1.0" encoding="UTF-8"?>'
header = ''
counter = 0;

CSV.foreach("xpath.csv", encoding: "iso-8859-1:UTF-8") do |row|
  if counter == 0
    header = row
    counter = counter + 1
  else
    stripedRow = row[4].strip
    if stripedRow != 'PagingXpath' and stripedRow != 'ProductButton'.strip and stripedRow != 'MainUrl'
      puts '<rule>'
      puts '<RuleId>'
      puts "#{row[0]}"
      puts '</RuleId>'
      puts '<SiteId>'
      puts "#{row[1]}"
      puts '</SiteId>'
      puts '<ProductTypeId>'
      puts "#{row[2]}"
      puts '</ProductTypeId>'
      puts '<XPath>'
      puts "#{row[3]}"
      puts '</XPath>'
      puts '<PropName>'
      puts "#{row[4]}"    
      puts '</PropName>'
      puts '<RuleIdRef>'
      puts "#{row[5]}"    
      puts '</RuleIdRef>'
      puts '</rule>'
    end
  end
end
