require 'csv'

puts '<?xml version="1.0" encoding="UTF-8"?>'
header = ''
counter = 0

siteId = ''
productId = ''
pagingXpath = ''
productButtonXpath = ''
mainUrl = ''


CSV.foreach("xpath.csv", encoding: "iso-8859-1:UTF-8") do |row|
  if counter == 0
    header = row
  else
    siteId = row[1]
	productId = row[2]
    if (counter%21 == 0)
	  pagingXpath = row[3]
    end
	
	if(counter%22 == 0)
      productButtonXpath = row[3]
	end
	
	if(counter%23 == 0)
      mainUrl = row[3]
	end
		if(!siteId.empty? and !productId.empty? and !pagingXpath.empty? and !productButtonXpath.empty? and !mainUrl.empty? )
      puts '<SiteProduct>'
        puts '<SiteId>'
          puts siteId
        puts '</SiteId>'  
		puts "<ProductId>"
	     puts productId
	    puts "</ProductId>"
	    puts '<PagingXpath>'
          puts pagingXpath
        puts '</PagingXpath>'
	  puts '<ProductButtonXpath>'
        puts productButtonXpath
      puts '</ProductButtonXpath>'			
	  puts '<MainUrl>'
        puts mainUrl
      puts '</MainUrl>'			  
      puts '</SiteProduct>'
	  
	  siteId = ''
      productId = ''
      pagingXpath = ''
      productButtonXpath = ''
      mainUrl = ''
	  end

    end
	    counter = counter + 1
  end

